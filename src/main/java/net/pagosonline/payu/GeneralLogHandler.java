package main.java.net.pagosonline.payu;


import org.apache.axis.AxisFault;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;
import org.apache.axis.handlers.LogHandler;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;


public class GeneralLogHandler extends LogHandler {

	private static Logger LOG= Logger.getLogger(GeneralLogHandler.class);


    public void invoke(MessageContext mc) throws AxisFault {
		if (mc.getResponseMessage() != null && mc.getResponseMessage().getSOAPPartAsString() != null) {
			String resMsg = mc.getResponseMessage().getSOAPPartAsString();
			System.out.println("Response: \r\n" + resMsg);
		} else if (mc.getRequestMessage() != null && mc.getRequestMessage().getSOAPPartAsString() != null) {
			String reqMsg = mc.getRequestMessage().getSOAPPartAsString();
			System.out.println("Request: \r\n" + reqMsg);
			LOG.info("Request: " + reqMsg);
		}
	}
}