package main.java.net.pagosonline.payu;


import org.apache.axis.AxisFault;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;


import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.soap.*;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;

import javax.xml.crypto.dsig.XMLSignatureFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class RequestHandler extends BasicHandler {


    @Override
    public void invoke(MessageContext messageContext) throws AxisFault {
        System.out.println("Inside handleRequest");
        SOAPElement soapHeader = null;
        try {

            SOAPMessage soapMessage = messageContext.getMessage();
            soapHeader = messageContext.getRequestMessage().getSOAPHeader();

            SOAPElement securityElement = soapHeader.addChildElement("Security",
                    "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");


            securityElement.addNamespaceDeclaration("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

            // (i) Extract the certificate from the .p12 file.
            Certificate cert = getCertificate();

            SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
            SOAPBody soapBody = soapMessage.getSOAPBody();

            soapBody.addAttribute(soapEnvelope.createName("Id", "wsu",
                    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"), "Body");

            //(iii) Add Timestamp element
            SOAPElement timestamp = addTimeStamp(securityElement, soapMessage);

            addBinarySecurityToken(securityElement, cert);

            // (iv) Add signature element
            addSignature(securityElement, soapBody, timestamp, cert);
        } catch (SOAPException | AlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private SOAPElement addSignature(
            SOAPElement securityElement, SOAPBody soapBody, SOAPElement timestamp, Certificate certificate) throws Exception {


        // Get private key from ROS digital certificate
        PrivateKey key = getKeyFormCert();


        SOAPElement securityTokenReference = addSecurityToken(securityElement);


        // Add signature
        createDetachedSignature(securityElement, key, securityTokenReference, soapBody, timestamp);

        return securityElement;

    }

    private SOAPElement addSecurityToken(SOAPElement signature) throws Exception {
        SOAPElement securityTokenReference = signature.addChildElement("SecurityTokenReference", "wsse");
        SOAPElement reference = securityTokenReference.addChildElement("Reference", "wsse");

        reference.setAttribute("URI", "#X509Token");

        return securityTokenReference;
    }

    private void addBinarySecurityToken(SOAPElement securityElement, Certificate cert) throws Exception {

        // Get byte array of cert.
        byte[] certByte = cert.getEncoded();

        // Add the Binary Security Token element
        SOAPElement binarySecurityToken = securityElement.addChildElement("KeyIdentifier", "wsse");
        binarySecurityToken.setAttribute("ValueType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3");
        binarySecurityToken.setAttribute("EncodingType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");
        binarySecurityToken.setAttribute("wsu:Id", "X509Token");
        binarySecurityToken.addTextNode(Base64.getEncoder().encodeToString(certByte));
    }

    private void createDetachedSignature(SOAPElement signatureElement, PrivateKey privateKey, SOAPElement securityTokenReference, SOAPBody soapBody, SOAPElement timestamp) {

        try {
            String providerName = System.getProperty
                    ("jsr105Provider", "org.jcp.xml.dsig.internal.dom.XMLDSigRI");
            XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM",
                    (Provider) Class.forName(providerName).newInstance());


            //Digest method
            javax.xml.crypto.dsig.DigestMethod digestMethod = xmlSignatureFactory.
                    newDigestMethod("http://www.w3.org/2001/04/xmlenc#sha512", null);
            ArrayList<Transform> transformList = new ArrayList<Transform>();


            //Transform
            Transform envTransform = xmlSignatureFactory.newTransform("http://www.w3.org/2001/10/xml-exc-c14n#", (TransformParameterSpec) null );

            transformList.add(envTransform);


            //References

            ArrayList<Reference> refList = new ArrayList<Reference>();
            Reference refTS = xmlSignatureFactory.newReference("#TS", digestMethod, transformList, null, null);
            Reference refBody = xmlSignatureFactory.newReference("#Body", digestMethod, transformList, null, null);


            refList.add(refBody);
            refList.add(refTS);



            javax.xml.crypto.dsig.CanonicalizationMethod cm = xmlSignatureFactory
                    .newCanonicalizationMethod("http://www.w3.org/2001/10/xml-exc-c14n#",
                    (C14NMethodParameterSpec) null);

            javax.xml.crypto.dsig.SignatureMethod sm = xmlSignatureFactory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha512", null);




            SignedInfo signedInfo = xmlSignatureFactory.newSignedInfo(cm, sm, refList );



            DOMSignContext signContext = new DOMSignContext(privateKey, signatureElement);
            signContext.setDefaultNamespacePrefix("ds");
            signContext.putNamespacePrefix("http://www.w3.org/2000/09/xmldsig#", "ds");


            //These are required for new Java versions
            signContext.setIdAttributeNS
                    (soapBody,
                            "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "Id");


            signContext.setIdAttributeNS
                    (timestamp,
                            "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "Id");


            KeyInfoFactory keyFactory = KeyInfoFactory.getInstance();
            DOMStructure domKeyInfo = new DOMStructure(securityTokenReference);
            javax.xml.crypto.dsig.keyinfo.KeyInfo keyInfo = keyFactory.newKeyInfo(java.util.Collections.singletonList(domKeyInfo));
            javax.xml.crypto.dsig.XMLSignature signature = xmlSignatureFactory.newXMLSignature(signedInfo, keyInfo);

            signContext.setBaseURI("");


            signature.sign(signContext);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private PrivateKey getKeyFormCert() throws Exception {

        String password = "password";

        // Get cert password.
        // (i) Get byte array of password
        byte[] passwordByte = password.getBytes();

        // (ii) Get MD5 Hash of byte array
        java.security.MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
        byte[] passwordHashed = digest.digest(passwordByte);

        // (iii) Base64 encode hashed byte array
        String passwordHashedBase64 = Base64.getEncoder().encodeToString(passwordHashed);

        // (iv) Open the cert using KeyStore
        KeyStore keystore = KeyStore.getInstance("PKCS12");
        String baseDir = System.getProperty("user.dir");
        keystore.load(new FileInputStream(new File(baseDir + "\\src\\main\\resources\\security_two\\testing\\llave.p12")), password.toCharArray());

        // (v) Extract Private Key
        PrivateKey key = (PrivateKey) keystore.getKey("pol", password.toCharArray());

        return key;

    }

    private Certificate getCertificate() throws AlgorithmException {
        // TODO : change to a file properites

        String password = "password";

        // (i) Get byte array of password
        byte[] passwordByte = password.getBytes();

        // (ii) Get MD5 Hash of byte array
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] passwordHashed = digest.digest(passwordByte);

            // (iii) Base64 encode hashed byte array
            String passwordHashedbase64 = Base64.getEncoder().encodeToString(passwordHashed);

            // (iv) Open the Seat using KeyStore
            KeyStore keystore = KeyStore.getInstance("PKCS12");
            String baseDir = System.getProperty("user.dir");
            keystore.load(new FileInputStream(new File(baseDir + "\\src\\main\\resources\\security_two\\testing\\llave.p12")), password.toCharArray());

            // (v) Extract the certificate.
            Certificate cert = keystore.getCertificate("pol");
            System.out.println();
            return cert;
        } catch (KeyStoreException | NoSuchFieldError | NoSuchAlgorithmException | FileNotFoundException e) {
            e.printStackTrace();


            throw new AlgorithmException(String.format("No se pudo cargar el certificado {}", e.getMessage()));
        } catch (CertificateException e) {
            e.printStackTrace();

            throw new AlgorithmException(String.format("No se pudo cargar el certificado {}", e.getMessage()));
        } catch (IOException e) {
            e.printStackTrace();
            throw new AlgorithmException(String.format("No se pudo cargar el certificado {}", e.getMessage()));
        }
    }

    private SOAPElement addTimeStamp(SOAPElement securityElement, SOAPMessage soapMessage) throws SOAPException {

        SOAPElement timestamp = securityElement.addChildElement("Timestamp", "wsu");
        SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
        timestamp.addAttribute(soapEnvelope.createName("Id", "wsu",
                "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"), "#TS");

        String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
        DateTimeFormatter timeStampFormatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
        timestamp.addChildElement("Created", "wsu").setValue(timeStampFormatter.format(ZonedDateTime.now().toInstant().atZone(ZoneId.of("UTC"))));
        timestamp.addChildElement("Expires", "wsu").setValue(timeStampFormatter.format(ZonedDateTime.now().plusSeconds(30).toInstant().atZone(ZoneId.of("UTC"))));
        return timestamp;

    }


}
